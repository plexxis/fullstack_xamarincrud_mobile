﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Collections.ObjectModel;


namespace app
{

    public partial class MainPage : ContentPage
    {
        string url = "http://10.0.2.2:8000/api/employee";
        EmployeesList plexxisEmployees;
        public class Employee
        {

            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string Profession { get; set; }
            public string Color { get; set; }
            public string City { get; set; }
            public string Branch { get; set; }
            public bool Assigned { get; set; }
        }
        public class EmployeesList
        {
            public ObservableCollection<Employee> employees { get; set; }

            public int count { get; set; }
        }
        public MainPage()
        {
            InitializeComponent();
            fetchEmployeeData(url);
        }

        public void fetchEmployeeData(string url)
        {
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";

            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
                System.Diagnostics.Debug.WriteLine(text);
                plexxisEmployees = JsonConvert.DeserializeObject<EmployeesList>(text);

                EmployeeView.ItemsSource = plexxisEmployees.employees;
            }
        }
    }
}
